#include "EntityInstance.h"
EntityInstance::EntityInstance(void)
{

}

EntityInstance::EntityInstance(shared_ptr<IEntity> entity, HookPackage hook_package, LogStream* log_stream)
{
	name_ = entity->getName();
	entity_ = entity;
	hook_package_ = hook_package;
	log_stream_ = log_stream;
}

EntityInstance::~EntityInstance(void)
{

}

void EntityInstance::AssignEntity(shared_ptr<IEntity> entity)
{
	entity_ = entity;
}

void EntityInstance::Update(double delta)
{

}

shared_ptr<IEntity> EntityInstance::GetPointer()
{
	return entity_;
}

vector<shared_ptr<IComponent>> EntityInstance::GetComponents()
{
	return components_;
}

string EntityInstance::Name()
{
	return name_;
}

shared_ptr<EntityInstance> EntityInstance::FindEntityWithName(string name)
{
	for (int i = 0; i < entity_list_->size(); i++)
	{
		if ((*entity_list_)[i].Name() == name)
		{
			// Creates a shared pointer to the EntityInstance in the list.
			EntityInstance test = (*entity_list_)[i];
			return make_shared<EntityInstance>(test);
		}
	}
	return nullptr;
}

void AssignScenario(shared_ptr<IScenario> scenario)
{

}