/*----------------------------Zach Howard-Smith----------------------------
This class is used to introduce an Entity Component Model paradigm to the
Titan gameplay. It stores a pointer to the entity in question, and a list of
polymorphic components to be executed.
-------------------------------------------------------------------------*/

#pragma once
//
// API
//
#include "titan/plugin2/IEntity.h"

//
// STD Library
//
#include <vector>

//
// Custom
//
#include "IComponent.h"
#include "HookPackage.h"
#include "LogStream.h"

//
// Namespace requirements
//
using namespace std;
using namespace nsc;
using namespace nsc::utility;

namespace nsc
{
	using namespace titan::api2;
	class EntityInstance
	{
		// Provides required access to the IScenario to allow updates of components automatically.
		friend class IScenario;

	public:
		EntityInstance(void);
		EntityInstance(shared_ptr<IEntity> entity, HookPackage hook_package, LogStream* log_stream);
		~EntityInstance(void);

		// Assigns an entity to the IEntity pointer ready
		// for usage by the EntityInstance and its components.
		void AssignEntity(shared_ptr<IEntity> entity);

		// Updates each of the components contained within the EntityInstance
		void Update(double delta);

		// Gets the pointer to the IEntity that the Entity instance holds.
		shared_ptr<IEntity> GetPointer();

		// Returns all components on the EntityInstance
		vector<shared_ptr<IComponent>> GetComponents();

		// Gets the name of the IEntity
		string Name();

		// Retrieves a pointer to the first EntityInstance with a matching name.
		shared_ptr<EntityInstance> FindEntityWithName(string name);

		inline void SetEntityList(shared_ptr<vector<EntityInstance>> entity_list) { entity_list_ = entity_list; }

		void AssignScenario(shared_ptr<IScenario> scenario);
#pragma region Templated Functions
		// Adds a new instance of a component.
		template<class T>
		void AddComponent(shared_ptr<IEntity> entity)
		{
			// Create a new instance of type T and generates a shared_ptr to it,
			// before pushing it onto the components list if it is valid.
			shared_ptr<IComponent> component_shared(new T(entity, hook_package_));

			component_shared->SetParent(make_shared<EntityInstance>(*this));
			components_.push_back(component_shared);
		}

		// Attempts to return the first instance of a component of type T.
		// Returns null if no matching component can be found.
		template <class T>
		shared_ptr<IComponent> GetComponent()
		{
			for each(shared_ptr<IComponent> component in components_)
			{
				IComponent* ptr = component.get();
				// Is the current component of type T
				if(dynamic_cast<T*>(ptr) != nullptr)
				{
					return component;
				}
			}
			return nullptr;
		}

		// Attempts to find a component of type T and remove it.
		template <class T>
		void RemoveComponent()
		{
			int removeIndex = -1;
			for(int i = 0; i < components_.size(); i++)
			{
				// Does the current component cast to a pointer of type T
				IComponent* ptr = components_[i].get();
				// Is the current component of type T
				if(dynamic_cast<T*>(ptr) != nullptr)
				{
					removeIndex = i;
					break; //We've found one. No point searching further
				}
			}

			// Must be removed after the iteration to prevent issue of modifying a
			// vector during iteration.
			if(removeIndex != -1)
			{
				// Get an iterator to the selected index and remove it
				auto iter = std::find(components_.begin(), components_.end(), components_[removeIndex]);
				components_.erase(iter);
			}
		}



#pragma endregion

	private:
		shared_ptr<vector<EntityInstance>> entity_list_;

		string name_;
		shared_ptr<IEntity> entity_;
		vector<shared_ptr<IComponent>> components_;
		HookPackage hook_package_;
		LogStream* log_stream_;
	};
}

