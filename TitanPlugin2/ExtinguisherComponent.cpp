#include "ExtinguisherComponent.h"


ExtinguisherComponent::ExtinguisherComponent(shared_ptr<IEntity> entity, HookPackage hook_package)
{
	entity_ = entity;
	hook_package_ = hook_package;
	exploded = false;
}


ExtinguisherComponent::~ExtinguisherComponent(void)
{
}


void ExtinguisherComponent::Start()
{
	log_stream_->Log("Started Extinguisher.");
}

void ExtinguisherComponent::Update(double delta)
{
	if (log_stream_ != nullptr)
	{
		
	}
	if (entity_->getDamageModel()->getHealthNormalized() < 0.1f && !exploded)
	{
		log_stream_->Log("Triggered.");
		Explode();
		exploded = true;
	}
}

void ExtinguisherComponent::Assign(LogStream* log_stream)
{
	log_stream_ = log_stream;
}

void ExtinguisherComponent::Explode()
{
	log_stream_->Log("Exploding.");
	EntityDescriptor munitionDescriptor = hook_package_.GetWorldHook()->getEntityDescriptor("grey_smoke_small");

	shared_ptr<IDamageModel> damageModel = entity_->getDamageModel();
	if (damageModel)
	{
		Vec3d entityPos = entity_->getPosition();
		Vec3d entityWorldUp = MathHelpers::normalize(entityPos);
		Vec3d detonatePos = entityPos + (entityWorldUp * 2.0);

		hook_package_.GetScenarioHook()->createEntity(munitionDescriptor, entityPos, entity_->getWorldRotation());
	}
	
}
void ExtinguisherComponent::Print()
{
	log_stream_->Log("Woooo it worked.");
}