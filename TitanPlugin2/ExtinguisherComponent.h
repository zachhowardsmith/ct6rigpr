/*----------------------------Zach Howard-Smith----------------------------
This class is used as a component that can be assigned to an EntityInstance,
and allows the effect of exploding when shot. This is to be assigned to
fire extinguishers within the level as an added effect.
-------------------------------------------------------------------------*/

#pragma once
//
// API
//
#include "titan/plugin2/ITitan.h"
#include "titan/plugin2/IDamageModel.h"
#include "titan/plugin2/IScenarioManager.h"
#include "titan/plugin2/IWorldManager.h"
#include "titan/plugin2/MathHelpers.h"

//
// STD Library
//

//
// Custom
//
#include "IComponent.h"
#include "LogStream.h"
#include "HookPackage.h"

//
// Namespace requirements
//
using namespace nsc;
using namespace nsc::utility;
using namespace titan::api2;

namespace nsc
{
	class ExtinguisherComponent : public IComponent
	{
	public:
		ExtinguisherComponent(shared_ptr<IEntity> entity, HookPackage hook_package);
		~ExtinguisherComponent(void);

		//Overriders
		void Start();
		void Update(double delta);
		void Assign(LogStream* log_stream);

		void Explode();
		void Print();
	private:

		int i;
		bool x;
		LogStream* log_stream_;

		bool exploded;

		HookPackage hook_package_;
	};
}
