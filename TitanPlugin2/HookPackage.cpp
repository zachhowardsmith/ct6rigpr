#include "HookPackage.h"


HookPackage::HookPackage()
{
	validated_ = false;
}

void HookPackage::Initialize(shared_ptr<ITitan> titan_hook)
{
	titan_hook_ = titan_hook;

	debrief_hook_ = titan_hook_->getDebriefManager();
	event_hook_ = titan_hook_->getEventManager();
	render_hook_ = titan_hook_->getRenderManager();
	scenario_hook_ = titan_hook_->getScenarioManager();
	simulation_hook_ = titan_hook_->getSimulationManager();
	world_hook_ = titan_hook_->getWorldManager();

	validated_ = true;

	// Perform validation checks.
	validated_ &= (debrief_hook_ != nullptr);
	validated_ &= (event_hook_ != nullptr);
	validated_ &= (render_hook_ != nullptr);
	validated_ &= (scenario_hook_ != nullptr);
	validated_ &= (simulation_hook_ != nullptr);
	validated_ &= (world_hook_ != nullptr);
}

bool HookPackage::Validated()
{
	return validated_;
}

shared_ptr<ITitan> HookPackage::GetTitanHook()
{
	return titan_hook_;
}

shared_ptr<IDebriefManager> HookPackage::GetDebriefHook()
{
	return debrief_hook_;
}

shared_ptr<IEventManager> HookPackage::GetEventHook()
{
	return event_hook_;
}

shared_ptr<IRenderManager> HookPackage::GetRenderHook()
{
	return render_hook_;
}

shared_ptr<IScenarioManager> HookPackage::GetScenarioHook()
{
	return scenario_hook_;
}

shared_ptr<ISimulationManager> HookPackage::GetSimulationHook()
{
	return simulation_hook_;
}

shared_ptr<IWorldManager> HookPackage::GetWorldHook()
{
	return world_hook_;
}

HookPackage::~HookPackage()
{
	if (debrief_hook_) debrief_hook_ = nullptr;
	if (event_hook_) event_hook_ = nullptr;
	if (render_hook_) render_hook_ = nullptr;
	if (scenario_hook_) scenario_hook_ = nullptr;
	if (simulation_hook_) simulation_hook_ = nullptr;
	if (world_hook_) world_hook_ = nullptr;
}
