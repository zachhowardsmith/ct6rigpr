/*----------------------------Zach Howard-Smith----------------------------
This object is used to pass pointers to all hooks to the Titan engine. This
is safer than utilising a global static variable to be accessed by all classes
and is quite speed efficient due only being transferred once per object that
will contain it.
-------------------------------------------------------------------------*/
#pragma once
//
// API
//
#include "titan/plugin2/IScenarioManager.h"
#include "titan/plugin2/ISimulationManager.h"
#include "titan/plugin2/IWorldManager.h"
#include "titan/plugin2/IDamageModel.h"
#include "titan/plugin2/IEntity.h"
#include "titan/plugin2/ITitan.h"
#include "titan/plugin2/MathHelpers.h"

//
// STD Library
//

//
// Custom
//

//
// Namespace requirements
//
using namespace titan::api2;
using namespace std;

class HookPackage
{
public:
	HookPackage();
	~HookPackage();

	// Parameter of type ITitan requires a valid argument to passed into it
	// to configure the remainder of the hooks into titan.
	void Initialize(shared_ptr<ITitan> titan_hook);

	// Gets a boolean representing a validation state
	bool Validated();

#pragma region Getters
	shared_ptr<ITitan> GetTitanHook();
	shared_ptr<IDebriefManager> GetDebriefHook();
	shared_ptr<IEventManager> GetEventHook();
	shared_ptr<IRenderManager> GetRenderHook();
	shared_ptr<IScenarioManager> GetScenarioHook();
	shared_ptr<ISimulationManager> GetSimulationHook();
	shared_ptr<IWorldManager> GetWorldHook();
#pragma endregion

private:
	shared_ptr<ITitan> titan_hook_;
	shared_ptr<IDebriefManager> debrief_hook_;
	shared_ptr<IEventManager> event_hook_;
	shared_ptr<IRenderManager> render_hook_;
	shared_ptr<IScenarioManager> scenario_hook_;
	shared_ptr<ISimulationManager> simulation_hook_;
	shared_ptr<IWorldManager> world_hook_;
	bool validated_;
};

