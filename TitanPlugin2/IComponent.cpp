#include "IComponent.h"

using namespace nsc;
IComponent::~IComponent(void) 
{ 
}
//
// Sets the parent entity of the component.
void IComponent::SetParent(shared_ptr<EntityInstance> parent)
{
	parent_ = parent;
}