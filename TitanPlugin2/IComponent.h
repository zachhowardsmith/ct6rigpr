/*----------------------------Zach Howard-Smith----------------------------
This class acts as the base class for polymorphic components, allowing for
variable behaviour across Titan entities.
-------------------------------------------------------------------------*/
#pragma once
//
// API
//
#include "titan/plugin2/IEntity.h"

//
// STD Library
//

//
// Custom
//
#include "LogStream.h"

//
// Namespace requirements
//
using namespace titan::api2;

namespace nsc
{
	class EntityInstance;
	class IComponent
	{
	protected:
		// Forward declaration allows the usage of the currently un-linked EntityInstance.
		// EntityInstance relies upon IComponent, while IComponent relies on EntityInstance.
		// This causes a circular include that fails compilation. Forward declaration
		// resolves this.

	public:
		virtual ~IComponent(void) = 0;

		// Will be called upon creation of a component.
		virtual void Start() = 0;

		// Will be called once per frame, allowing the component to perform actions.
		virtual void Update(double delta) = 0;

		// Assigns a LogStream to allow for debugging.
		virtual void Assign(LogStream* log_stream) = 0;

		// Sets the parent entity of the component.
		void SetParent(shared_ptr<EntityInstance> parent);
	protected:
		shared_ptr<IEntity> entity_;
		shared_ptr<EntityInstance> parent_;
	};
}