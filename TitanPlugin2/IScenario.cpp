#include "IScenario.h"

IScenario::~IScenario()
{
}

void IScenario::InitializeHooks(shared_ptr<ITitan> titan_hook)
{
	log_stream_->Log("Constructing API hooks.");
	hook_package_.Initialize(titan_hook);

	validation_status_ = hook_package_.Validated();
	string resultant = (validation_status_ == true ? "" : "un");
	log_stream_->Log("Hook construction was " + resultant + "successful.");
}

bool IScenario::Validated()
{
	return validation_status_;
}

void IScenario::UpdateEntityComponents(double delta)
{
	for (EntityInstance entity : entities_)
		for (shared_ptr<IComponent> component : entity.components_)
			component->Update(delta);
}

void IScenario::StartEntityComponents()
{
	for (EntityInstance entity : entities_)
		for (shared_ptr<IComponent> component : entity.components_)
		{
			component->Assign(log_stream_);
			component->Start();
		}
}

vector<EntityInstance> IScenario::GetEntities()
{
	return entities_;
}