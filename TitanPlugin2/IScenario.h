/*----------------------------Zach Howard-Smith----------------------------
This class acts as the base class for Titan scenarios, using polymorphism
to allow the plugin to run different scenarios with little overhead for the
programmer. It retrieves all of the required hooks before passing to the
derivations. This polymophism allows for "levels" to be introduced through
dynamic switching of the current_scenario variable in TitanPlugin.cpp.
-------------------------------------------------------------------------*/

#pragma once
//
// API
//
#include "titan/plugin2/IScenarioManager.h"
#include "titan/plugin2/ISimulationManager.h"
#include "titan/plugin2/IWorldManager.h"
#include "titan/plugin2/IDamageModel.h"
#include "titan/plugin2/IEntity.h"
#include "titan/plugin2/ITitan.h"
#include "titan/plugin2/MathHelpers.h"

//
// STD Library
//
#include <vector>

//
// Custom
//
#include "Timer.h"
#include "Utility.h"
#include "LogStream.h"
#include "EntityInstance.h"
#include "HookPackage.h"

//
// Namespace requirements
//
using namespace std;
using namespace titan::api2;
using namespace nsc;
using namespace nsc::utility;

namespace nsc
{
	class IScenario
	{
	public:
#pragma region Public Functions
		virtual ~IScenario() = 0;

		// Called upon creation of the scenario to configure any required
		// memory access.
		virtual void Initialize() = 0;

		// Called when the scenario starts to run.
		virtual void Start() = 0;

		// Will be called once per frame to allow the scenario to update
		// all the entities required.
		virtual void Update(double delta) = 0;

		// Will be called upon termination of the scenario to clean up
		// any required memory.
		virtual void Terminate() = 0;

		// Builds the scenario's timers and configures the callbacks.
		virtual void InitializeTimers() = 0;

		// Should be called within the scenario's Start() 
		// and should populate the entities_ variable with
		// a list of entities that will be required by the plugin.
		virtual void PopulateEntities() = 0;

		// Initialize all of the hooks in to the API that the scenario
		// may need to function correctly.
		void InitializeHooks(shared_ptr<ITitan> titanHook);

		// Retrieves a validation status in the form of a boolean, 
		// indicating whether all of the hooks have been correctly
		// attached.
		bool Validated();

		// Updates all of the entities within the scenario.
		void UpdateEntityComponents(double delta);

		// Updates all of the entities within the scenario.
		void StartEntityComponents();

		// Gets a list of the entities populated by the scenario.
		vector<EntityInstance> GetEntities();
		
#pragma endregion

	protected:
#pragma region Protected Functions
		
		// Should be called within the scenario's Update() to ensure
		// timers are being processed.
		virtual void ScanTimers(double delta) = 0;

#pragma endregion	
#pragma region Protected Variables
		//
		// Utility
		//
		vector<Timer> timers;
		vector<short> callback_flags_; // Used to indicate a callback from the timer
		LogStream* log_stream_;

		// API Hooks
		HookPackage hook_package_;
		bool validation_status_;

		//
		// Scenario 
		//
		bool scenario_playing_;
		vector<EntityInstance> entities_;
		shared_ptr<EntityInstance> parent_;

#pragma endregion
	};
}
