#include "LogStream.h"
LogStream::LogStream()
{
}


LogStream::~LogStream()
{
}

void LogStream::Initialize()
{
	logger.open(kPluginName + ".log");
}

void LogStream::Log(string logVal)
{
	time_t time_create = time(NULL);
	struct tm now;
	localtime_s(&now, &time_create);
	logger << "[TCIT " << now.tm_mday << "/" << now.tm_mon << "/" << now.tm_year << " - " << now.tm_hour << ":" << now.tm_min << ":" << now.tm_sec << "] ";
	logger << logVal << endl;
}

void LogStream::Terminate()
{
	logger.close();
}