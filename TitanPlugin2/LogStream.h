/*----------------------------Zach Howard-Smith----------------------------
This object is used to open a filestream to a log file within the Titan 
directory.
-------------------------------------------------------------------------*/
#pragma once
//
// API
//

//
// STD Library
//
#include <fstream>
#include <iostream>
#include <time.h>

//
// Custom
//
#include "Utility.h"

//
// Namespace requirements
//
using namespace std;
using namespace nsc::utility;

namespace nsc
{
	namespace utility
	{
		class LogStream
		{
		public:
			LogStream();
			~LogStream();

			// Configures the log stream to alllow for file writing.
			void Initialize();

			// Logs a string to the open stream.
			void Log(string logVal);

			// Closes down the logger.
			void Terminate();

		private:
			//
			// Variables
			//
			ofstream logger;

		};
	}
}