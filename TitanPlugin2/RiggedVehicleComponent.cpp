#include "RiggedVehicleComponent.h"
#include "ExtinguisherComponent.h"
#include "EntityInstance.h"

RiggedVehicleComponent::RiggedVehicleComponent(shared_ptr<IEntity> entity, HookPackage hook_package)
{
	entity_ = entity;
	hook_package_ = hook_package;
	exploded = false;
}


RiggedVehicleComponent::~RiggedVehicleComponent(void)
{
}

void RiggedVehicleComponent::Start()
{
	position_ = entity_->getPosition();
	log_stream_->Log("Started Rigged Vehicle Component.");
}

void RiggedVehicleComponent::Update(double delta)
{
	if (MathHelpers::magnitude(position_ - entity_->getPosition()) > 1.0f && !exploded)
		Explode();

	ExtinguisherComponent* e = ((ExtinguisherComponent*)parent_->FindEntityWithName("a")->GetComponent<ExtinguisherComponent>().get());
	if(e != nullptr)
		e->Print();
	
}

void RiggedVehicleComponent::Assign(LogStream* log_stream)
{
	log_stream_ = log_stream;
}



void RiggedVehicleComponent::Explode()
{
	log_stream_->Log("Boom." + entity_->getName());
	EntityDescriptor munitionDescriptor = hook_package_.GetWorldHook()->getEntityDescriptor("gbu32");

	shared_ptr<IDamageModel> damageModel = entity_->getDamageModel();
	if (damageModel)
	{
		// If the entity is not dead
		if (damageModel->getHealthNormalized() > 0.0)
		{
			Vec3d entityPos = entity_->getPosition();
			Vec3d entityWorldUp = MathHelpers::normalize(entityPos);
			Vec3d detonatePos = entityPos + (entityWorldUp * 2.0);

			// Detonate a one-shot munition over it
			hook_package_.GetWorldHook()->detonateMunition(munitionDescriptor,
				detonatePos,
				entity_->getWorldRotation(),
				Vec3d(0, 0, 27),
				ImpactOrganic);
		}
		exploded = true;
	}
	
}