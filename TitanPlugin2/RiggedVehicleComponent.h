/*----------------------------Zach Howard-Smith----------------------------
This class is used as a component that can be assigned to an EntityInstance,
and allows the effective "rigging" of a vehicle, causing it to explode if it
is driven.
-------------------------------------------------------------------------*/

#pragma once
//
// API
//
#include "titan/plugin2/ITitan.h"
#include "titan/plugin2/IDamageModel.h"
#include "titan/plugin2/IScenarioManager.h"
#include "titan/plugin2/IWorldManager.h"
#include "titan/plugin2/MathHelpers.h"
#include "titan/plugin2/IVehicleTraits.h"

//
// STD Library
//

//
// Custom
//
#include "IComponent.h"
#include "LogStream.h"
#include "HookPackage.h"

//
// Namespace requirements
//
using namespace nsc;
using namespace nsc::utility;
using namespace titan::api2;

namespace nsc
{
	class RiggedVehicleComponent : public IComponent
	{
	public:
		RiggedVehicleComponent(shared_ptr<IEntity> entity, HookPackage hook_package);
		~RiggedVehicleComponent(void);

		//Overriders
		void Start();
		void Update(double delta);
		void Assign(LogStream* log_stream);

		void Explode();
	private:

		int i;
		bool x;
		LogStream* log_stream_;

		bool exploded;

		HookPackage hook_package_;
		Vec3d position_;
	};
}
