#include "ScenarioExplosion.h"

ScenarioExplosion::ScenarioExplosion(LogStream* log_stream)
{
	log_stream_ = log_stream;
}


ScenarioExplosion::~ScenarioExplosion()
{

}

void ScenarioExplosion::Initialize()
{

}

void ScenarioExplosion::Start()
{
	log_stream_->Log("Explosion scenario has begun.");
}

void ScenarioExplosion::Update(double delta)
{
	if (hook_package_.GetSimulationHook()->getSimulationMode() == SimulationMode::ModeAdmin || hook_package_.GetSimulationHook()->getSimulationMode() == SimulationMode::ModePlay)
	{
		ScanTimers(delta);
	}
	else
	{
		scenario_playing_ = false;
	}

}

void ScenarioExplosion::Terminate()
{

}

void ScenarioExplosion::InitializeTimers()
{
	log_stream_->Log("Constructing Timers.");
	timers.emplace_back();
	callback_flags_.push_back(false);

	timers[Explosion].Assign(log_stream_);
	timers[Explosion].Start(&(callback_flags_[Explosion]), 5.0f, true);
}


void ScenarioExplosion::PopulateEntities()
{
	set<shared_ptr<IEntity>> entities = hook_package_.GetScenarioHook()->getEntities();



	// Assigns a ptr to a list of entities to the entity to allow for
	// FindEntityWithName calls. Cannot currently be declared outside
	// of the loop as it induces a crash if no entities are within the scene.
	list_pointer_ = shared_ptr<vector<EntityInstance>>(&entities_);

	for(shared_ptr<IEntity> entity : entities)
	{
		EntityInstance new_entity(entity, hook_package_, log_stream_);
		new_entity.SetEntityList(list_pointer_);

		string name = entity->getName();
		string stringExt = "a";
		if (!name.compare(0, stringExt.size(), stringExt))
		{
			new_entity.AddComponent<ExtinguisherComponent>(entity);
			new_entity.GetComponent<ExtinguisherComponent>()->Assign(log_stream_);
		}

		stringExt = "b";
		if (!name.compare(0, stringExt.size(), stringExt))
		{
			new_entity.AddComponent<RiggedVehicleComponent>(entity);
			new_entity.GetComponent<RiggedVehicleComponent>()->Assign(log_stream_);
		}

		log_stream_->Log(entity->getName() + " was registered as a scenario entity.");


		entities_.push_back(new_entity);
	}
}

bool StartsWith(string str, string prefix)
{
}


void ScenarioExplosion::ScanTimers(double delta)
{
	for (uint i = 0; i < callback_flags_.size(); i++)
	{
		timers[i].Update(delta);
		if (callback_flags_[i] == 1)
		{
			callback_flags_[i] = 0;
			
		}
	}
}