/*----------------------------Zach Howard-Smith----------------------------
This class represents a gameplay scenario within titan.
-------------------------------------------------------------------------*/
#pragma once
//
// API
//
#include "titan/plugin2/IEntity.h"

//
// STD Library
//

//
// Custom
//
#include "LogStream.h"
#include "IScenario.h"
#include "ExtinguisherComponent.h"
#include "RiggedVehicleComponent.h"

//
// Namespace requirements
//
using namespace nsc;

namespace nsc
{
	class ScenarioExplosion final : public IScenario
	{

		enum TimerPurpose : int
		{
			Explosion
		};

	public:
		ScenarioExplosion(LogStream* log_stream);
		~ScenarioExplosion();

		void Initialize();
		void Start();
		void Update(double delta);
		void Terminate();
		void InitializeTimers();
		void PopulateEntities();

	private:
		void ScanTimers(double delta);

		vector<shared_ptr<IEntity>> terrorists_;
		vector<shared_ptr<IEntity>> hostages_;
		shared_ptr<vector<EntityInstance>> list_pointer_;

	};
}