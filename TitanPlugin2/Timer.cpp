#include "Timer.h"

Timer::Timer()
{
	duration_ = 0;
	current_ = 0;
	running_ = false;
	validated_ = false;
}


Timer::~Timer()
{

}

void Timer::Start(short* flag, double duration, bool auto_stop)
{
	current_ = 0;
	duration_ = duration;
	running_ = true;
	auto_stop_ = auto_stop;

	callback_flag_ = flag;
	*flag = 0;

	validated_ = true;
}

void Timer::Restart()
{
	// Ensures the timer cannot be started prior to recieving it's callback
	if(validated_)
		Start(callback_flag_, duration_, auto_stop_);
}

void Timer::Suspend()
{
	running_ = false;
}

void Timer::Resume()
{
	// Ensures the timer cannot be started prior to recieving it's callback
	if(validated_)
		running_ = false;
}

void Timer::Terminate()
{
	running_ = false;
	current_ = 0;
}

void Timer::Update(double delta)
{
	if (running_)
	{
		current_ += delta;

		// If the timer has exceeded it's duration, invoke the callback.
		if (current_ > duration_)
		{
			current_ = 0;
			running_ = !auto_stop_;
			*callback_flag_ = 1;
		}
	}
}

bool Timer::Running()
{
	return running_;
}


void Timer::Assign(LogStream* log_stream)
{
	log_stream_ = log_stream;
}