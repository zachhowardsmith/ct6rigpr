/*----------------------------Zach Howard-Smith----------------------------
This object is used to time events, providing a call back to the parent.
Function pointers to member functions were not available so the current
alternative is not as elegant as I would like. A replacement system will
be investigated in future.
-------------------------------------------------------------------------*/
#pragma once
//
// API
//

//
// STD Library
//

//
// Custom
//
#include "LogStream.h"

//
// Namespace requirements
//
using namespace nsc::utility;

namespace nsc
{
	namespace utility
	{
		class Timer
		{
		public:
			Timer();
			~Timer();

			// Starts the timer ticking.
			// flag is a pointer to a callback short that will be modified
			// upon a timer completion. This is a temporary method as callbacks
			// to member functions were not functional.
			void Start(short* flag, double duration, bool auto_stop = false);

			// Restarts a stopped timer with the same initialization values
			// as were provided when Start() was called.
			void Restart();
			
			// Suspends the timer in its current state.
			void Suspend();

			// Resumes the timer in its current state.
			void Resume();

			// Stops the timer instantly.
			void Terminate();

			// Used to update the timers current tick value.
			void Update(double delta);

			// Returns a boolean indicative of whether the time is running.
			bool Running();

			void Assign(LogStream* log_stream);
		private:
			double duration_;
			double current_;
			bool running_;
			bool auto_stop_;

			short* callback_flag_;

			bool validated_;

			LogStream* log_stream_;
		};
	}
}