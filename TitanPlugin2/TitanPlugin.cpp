#include "TitanPlugin.h"

using namespace nsc;

void pluginStartup(const shared_ptr<ITitan>& api)
{
	titan_hook = api;
	simulation_hook = api->getSimulationManager();
	log_stream.Initialize();

	log_stream.Log("Plugin initializing.");
	simulation_mode = SimulationMode::ModeNone;
	// Scenario Setup
}

void pluginUpdate(double delta)
{
	CheckStateChange();

	if (current_scenario != nullptr && current_scenario->Validated())
	{
		current_scenario->UpdateEntityComponents(delta);
		current_scenario->Update(delta);
	}
}

void pluginShutdown()
{
	// Ensure we don't have a dangling pointer
	if (titan_hook) titan_hook = nullptr;

	// Clean the scenario
	delete current_scenario;
	current_scenario = nullptr;

	// Clean the log stream
	log_stream.Terminate();
}


void InitializeScenario(bool auto_run = false)
{
	current_scenario = new ScenarioExplosion(&log_stream);
	log_stream.Log("Created Explosion Scenario.");

	current_scenario->InitializeHooks(titan_hook);
	current_scenario->Initialize();
	current_scenario->InitializeTimers();
	current_scenario->PopulateEntities();
	current_scenario->StartEntityComponents();

	if(auto_run)
		current_scenario->Start();
}

void CheckStateChange()
{
	SwitchState switch_state = GetSwitchState();

	switch (switch_state)
	{
	// Playable to non playable
	case kPlayToNone:
	case kPlayToMenu:
	case kPlayToEditor:
	case kAdminToNone:
	case kAdminToMenu:
	case kAdminToEditor:
	case kObserverToNone:
	case kObserverToMenu:
	case kObserverToEditor:
		log_stream.Log("Beginning scenario termination.");
		current_scenario->Terminate();
		delete current_scenario;
		current_scenario = nullptr;
		break;

	//Non playable to playable
	case kNoneToAdmin:
	case kNoneToObserver:
	case kNoneToPlay:
	case kMenuToAdmin:
	case kMenuToObserver:
	case kMenuToPlay:
	case kEditorToAdmin:
	case kEditorToObserver:
	case kEditorToPlay:	
		log_stream.Log("Beginning scenario initialization.");
		InitializeScenario(true);
		break;

	default:
		break;
	}

}

SwitchState GetSwitchState()
{
		SimulationMode new_mode = simulation_hook->getSimulationMode();

		// Calculates the value of the SwitchState enum from the
		// values of the old, and new simulation state enums.
		int enum_index = (7 * simulation_mode) + new_mode;
		simulation_mode = new_mode;
		return (SwitchState)enum_index;
}

