/*----------------------------Zach Howard-Smith----------------------------
This static context provides the interaction between the plugin and the API.
It is the plugin's "main".
-------------------------------------------------------------------------*/
#pragma once
#pragma region Includes and Usings
//
// API
//
#include "titan/plugin2/plugin.h"
#include "titan/plugin2/ISimulationManager.h"

//
// STD Library
//
#include <Windows.h>

//
// Custom
//
#include "IScenario.h"
#include "ScenarioExplosion.h"
#include "LogStream.h"
#include "EntityInstance.h"

//
// Namespace requirements
//
using namespace std;
using namespace titan::api2;
using namespace nsc;
#pragma endregion

#pragma region API Calls 
// Called by the API when the plugin is started.
void pluginStartup(const shared_ptr<ITitan>& api);

// Called by the API once per frame.
void pluginUpdate(double dt);

// Called by the API when the plugin is shut down.
void pluginShutdown();
#pragma endregion

#pragma region Custom Functionality
// Constructs a current scenario ready for using.
void InitializeScenario(bool auto_run);

// This function is used to detect changes between the playability state of the Titan instance.
// If this is not performed, Titan loads the API and executes it from the beginning of execution,
// and therefore the scenario begins immediately and functions as a run once scenario requiring a
// Titan restart to restart the scenario.
void CheckStateChange();


SwitchState GetSwitchState();

//
// Utility
//
#pragma endregion

#pragma region Declarations
//
// Plugin Members
//
IScenario* current_scenario;

//
// API Hooks
//
shared_ptr<ITitan> titan_hook;
shared_ptr<ISimulationManager> simulation_hook;

//
// Utility
//
LogStream log_stream;

//
// Plugin
//
SimulationMode simulation_mode;
#pragma endregion


