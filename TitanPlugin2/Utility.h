/*----------------------------Zach Howard-Smith----------------------------
This namespace contains a number of global members that are utilised by the
plugin.
-------------------------------------------------------------------------*/
#pragma once
//
// API
//
//
#include "titan/plugin2/ITitan.h"

// STD Library
//
#include <fstream>
#include <iostream>
#include <string>
#include <time.h>

//
// Custom
//

//
// Namespace requirements
//
using namespace std;
using namespace titan::api2;

namespace nsc
{
	namespace utility
	{
		//
		// Typedefs
		//
		typedef unsigned int uint;
		typedef void(*callback)(int);

		// 
		// Constants
		//
		static const string kPluginName = "ApplicationNSC";
		static const uint kScenarioCount = 2;

		//
		// Enumerations
		//
		enum ScenarioNames : int
		{
			kExplosion = 0,
			kHostage = 1
		};

		
		enum SwitchState
		{
			kNoneToNone,
			kNoneToMenu,
			kNoneToEditor,
			kNoneToAdmin,
			kNoneToObserver,
			kNoneToPlay,
			kNoneToScenarioPlayback,
	
			kMenuToNone,
			kMenuToMenu,
			kMenuToEditor,
			kMenuToAdmin,
			kMenuToObserver,
			kMenuToPlay,
			kMenuToScenarioPlayback,
	
			kEditorToNone,
			kEditorToMenu,
			kEditorToEditor,
			kEditorToAdmin,
			kEditorToObserver,
			kEditorToPlay,
			kEditorToScenarioPlayback,

			kAdminToNone,
			kAdminToMenu,
			kAdminToEditor,
			kAdminToAdmin,
			kAdminToObserver,
			kAdminToPlay,
			kAdminToScenarioPlayback,
	
			kObserverToNone,
			kObserverToMenu,
			kObserverToEditor,
			kObserverToAdmin,
			kObserverToObserver,
			kObserverToPlay,
			kObserverToScenarioPlayback,
	
			kPlayToNone,
			kPlayToMenu,
			kPlayToEditor,
			kPlayToAdmin,
			kPlayToObserver,
			kPlayToPlay,
			kPlayToScenarioPlayback,
	
			kScenarioPlaybackToNone,
			kScenarioPlaybackToMenu,
			kScenarioPlaybackToEditor,
			kScenarioPlaybackToAdmin,
			kScenarioPlaybackToObserver,
			kScenarioPlaybackToPlay,
			kScenarioPlaybackToScenarioPlayback
		};

		//
		// Variables
		//
	}
}